const TMI = require('tmi.js');
const Config = require('./config.json');
const envVariables = new Map;

process.argv.forEach(function (value) {
    if (value.includes("=")) {
        envVariables.set(
            value.substr(0, value.indexOf("=")),
            value
                .substr(value.indexOf("=") + 1, value.length - value.indexOf("=") + 1)
                .toString()
        );
    }
});


const loginOptions = {
    identity: {
        username: Config.username,
        password: envVariables.get("twitchPassword")
    },
    channels: [
        "sinsa92"
    ]
}

const client = new TMI.client(loginOptions);
client.on('connected', connectionEstablished);
client.on('message', messageReceived);
client.connect();

function connectionEstablished(address, port) {
    console.log(`Successfully connected to ${address}:${port}!`);
}

function messageReceived(channel, userstate, message, self) {
    if (self) return; // we don't need to react from messages from Rikona herself
    message = message.trim().split(" ");
    if (message.length > 1) {
        if (message[0] === "I'm" || message[0] === "Im" || message[0] === "I`m") {
            let response = `Hi `;
            for (let i = 1; i < message.length; i++) {
                response += `${message[i]}`;
                if(i<message.length-1) {
                    response+=' ';
                }
            }
            response += ", I'm dad!";
            client.say(channel.substring(1), response);
        }else if(message[0]==="I"&&message[1]==="am") {
            let response = `Hi `;
            for (let i = 2; i < message.length; i++) {
                response += `${message[i]} `;
                if(i<message.length-1) {
                    response+=' ';
                }
            }
            response += ", I'm dad!";
            client.say(channel.substring(1), response);
        }
    }

}